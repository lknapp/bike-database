# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2019_01_15_113555) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agencies", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.string "contact_name", limit: 255
    t.string "street_address", limit: 255
    t.string "city", limit: 255
    t.string "state", limit: 255
    t.string "postal_code", limit: 255
    t.string "phone", limit: 255
    t.string "email", limit: 255
  end

  create_table "bikes", id: :serial, force: :cascade do |t|
    t.string "brand", limit: 255
    t.string "model", limit: 255
    t.string "bike_type", limit: 255
    t.string "color", limit: 255
    t.string "serial_number", limit: 255
    t.text "work_done"
    t.text "new_parts"
    t.string "price", limit: 255
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.float "seat_tube_size"
    t.float "top_tube_size"
    t.integer "log_number"
    t.text "purpose"
    t.text "mechanic"
    t.datetime "date_sold", precision: nil
    t.integer "bike_index_id"
    t.datetime "fixed_at", precision: nil
    t.decimal "time_spent", precision: 10, scale: 2
    t.string "location"
  end

  create_table "clients", id: :serial, force: :cascade do |t|
    t.string "first_name", limit: 255
    t.string "last_name", limit: 255
    t.string "gender", limit: 255
    t.integer "age"
    t.boolean "helmet"
    t.boolean "lock"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "bike_id"
    t.string "bike_type_requested", limit: 255
    t.boolean "will_pay"
    t.integer "agency_id"
    t.text "notes"
    t.boolean "bike_fixed"
    t.boolean "application_voided"
    t.string "volunteer_at_pickup", limit: 255
    t.float "weight"
    t.float "height"
    t.datetime "application_date", precision: nil
    t.datetime "pickup_date", precision: nil
    t.datetime "assigned_bike_at", precision: nil
    t.index ["agency_id"], name: "index_clients_on_agency_id"
    t.index ["bike_id"], name: "index_clients_on_bike_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", limit: 255, default: "", null: false
    t.string "encrypted_password", limit: 255, default: "", null: false
    t.string "reset_password_token", limit: 255
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip", limit: 255
    t.string "last_sign_in_ip", limit: 255
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "volunteers", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.string "email", limit: 255
    t.string "phone", limit: 255
    t.date "orientation_date"
    t.integer "other_volunteer_hours"
    t.text "referral"
    t.text "reason"
    t.text "skills"
    t.text "wants"
    t.boolean "interested_in_improving"
    t.boolean "available_weekends"
    t.boolean "available_weekdays"
    t.boolean "available_shorter_hours"
    t.boolean "available_longer_hours"
    t.boolean "flexible"
    t.text "questions"
    t.text "improve_orientation"
  end

end
